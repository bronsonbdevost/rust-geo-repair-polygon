# rust-geo-repair-polygon

A package containing:
 
 * a trait for (Multi)Polygon that attempts repairing a geo-types (Multi)Polygon that is invalid according to `geo-validator`.
 * a trait for MultiPolygon that merges all of its Polygons into a single valid Polygon
 
## Repair (MultiPolygon/Polygon)

When running repair, it will try its best to produce a (Multi)Polygon that meets OGC standards. Some very invalid polygons may still fail, but most come through as valid with very little change.

### Example

```rust
use geo_repair_polygon::repair::Repair;
use geo_types::polygon;

let bowtie = polygon![
    (x: 0_f64, y: 0.),
    (x: 0., y: 20.),
    (x: 20., y: 0.),
    (x: 20., y: 20.),
    (x: 0., y: 0.)];

let expected = polygon![
    (x: 0_f64, y: 0.),
    (x: 9.999999999999996, y: 9.999999999999996),
    (x: 10.000000000000004, y: 9.999999999999996),
    (x: 20., y: 0.),
    (x: 20., y: 20.),
    (x: 10.000000000000004, y: 10.000000000000004),
    (x: 9.999999999999996, y: 10.000000000000004),
    (x: 0., y: 20.),
    (x: 0., y: 0.)];

let repaired_bowtie = bowtie.repair();
assert!(repaired_bowtie.is_some());
assert_eq!(repaired_bowtie.unwrap(), expected);
```

## Join (MultiPolygon)

The join trait for MultiPolygon will merge all of its Polygons into a single valid Polygon. This may involve a union or the creation of a small bridge between the closest points of non-overlapping Polygons.

### Example

``` rust
use geo_repair_polygon::join::Join;
use geo_types::{polygon, MultiPolygon};

let separate_polygons: MultiPolygon<f64> = vec![
    polygon![
        (x: 0_f64, y: 0.),
        (x: 10., y: 0.),
        (x: 10., y: 10.),
        (x: 0., y: 10.),
        (x: 0., y: 0.)],
    polygon![
        (x: 11_f64, y: 11.),
        (x: 20., y: 11.),
        (x: 20., y: 20.),
        (x: 11., y: 20.),
        (x: 11., y: 11.)],
    ].into();

let expected = polygon![
    (x: 10_f64, y: 10.),
    (x: 11., y: 11.),
    (x: 20., y: 11.),
    (x: 20., y: 20.),
    (x: 11., y: 20.),
    (x: 11., y: 11.000000000000004),
    (x: 9.999999999999996, y: 10.),
    (x: 0., y: 10.),
    (x: 0., y: 0.),
    (x: 10., y: 0.),
    (x: 10., y: 10.)];

let merged = separate_polygons.join();
assert_eq!(merged, expected);
```